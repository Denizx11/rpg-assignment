using RPG_Assignment;
using RPG_Assignment.Classes;

namespace RPG_Tester
{
    public class RPGTester
    {
        [Fact]
        public void CorrectName_returnSameName()
        {
            //arrange
            string Name = "Deniz";
            Mage hero = new Mage(Name);
            string expected = "Deniz";
            //Act
            string actual = hero.name;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CorrectLevel_returnFirstLevel()
        {
            //arrange
            string Name = "Deniz";
            Mage hero = new Mage(Name);
            int expected = 1;
            //Act
            int actual = hero.level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CorrectLevel_AfterLevelup_returnLevelTwo()
        {
            //arrange
            string Name = "Deniz";
            Mage hero = new Mage(Name);
            hero.LevelUp();
            int expected = 2;
            //Act
            int actual = hero.level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CorrectAttributesMage_returnMAgeAttributes()
        {
            //arrange
            string Name = "Deniz";
            Mage hero = new Mage(Name);
            HeroAtrributes ExpectedAttributes = new HeroAtrributes(1,1,8);
            //Act
            int actual = hero.level;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}