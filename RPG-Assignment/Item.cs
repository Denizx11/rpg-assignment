﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG_Assignment.Enums;

namespace RPG_Assignment
{
    public class Item
    {
        public Item(string name, int requiredLevel)
        {
            Name = name;
            RequiredLevel = requiredLevel;
        }

        public string Name { get; set; }
        private int RequiredLevel { get; init; }
        protected Slot Slot { get; set; }

        public bool isValidLevel(int CurrentLevel)
        {
            if (CurrentLevel >= RequiredLevel) return true;
            return false;
        }

    }
}
