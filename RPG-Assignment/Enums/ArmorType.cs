﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment.Enums
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
