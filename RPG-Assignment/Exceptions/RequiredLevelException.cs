﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment.Exceptions
{
    public class RequiredLevelException : Exception
    {
        public override string Message => "Hero's level is too low!";
    }
}
