﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment.Exceptions
{
    public class InvalidArmorException: Exception
    {
        public override string Message => "Hero cannot equip this Armor!";
    }
}
