﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG_Assignment.Enums;

namespace RPG_Assignment
{
    public class Weapon : Item
    {
        public Weapon(string name, int requiredLevel, WeaponType weapon, int weaponDamage) : base(name, requiredLevel)
        {
            Slot = Slot.Weapon;
            WeaponType = weapon;
            WeaponDamage = weaponDamage;
        }

        public int WeaponDamage { get; set; } = 1;
        public WeaponType? WeaponType { get; set; } = null;

        public Slot GetSlot()
        {
            return Slot;
        }
   

    }
}
