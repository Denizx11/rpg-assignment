﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment
{
    public class HeroAtrributes
    {
        public int Strength { get; set; } = 0;
        public int Dexterity { get; set; } = 0;
        public int Intelligence { get; set; } = 0;

        public HeroAtrributes()
        { 
        }
        public HeroAtrributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public void LevelUp(int strength=0, int dexterity=0,int intelligence = 0)
        {
            Strength += strength;
            Dexterity += dexterity;
            Intelligence += intelligence;
        }

        public int GetTotalAttribute()
        {
            return Strength + Dexterity + Intelligence;
        }
    }
}
