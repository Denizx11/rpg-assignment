﻿using RPG_Assignment.Classes;
using RPG_Assignment.Enums;
using RPG_Assignment.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment
{
    public abstract class Hero
    {
        public string name { get; init; }
        public int level {  get; set; } = 0;
        public HeroAtrributes LevelAttribute = new HeroAtrributes();

        public Dictionary<Slot, Item?> Equipments { get; set; }
        public List<WeaponType> ValidWeaponTypes { get; }
        public List<ArmorType> ValidArmorTypes { get; }

        protected ClassName className { get; init; }


        public Hero(string Name)
        {
            name = Name;
            Equipments = new Dictionary<Slot, Item?>();
            ValidWeaponTypes = new List<WeaponType>();
            ValidArmorTypes = new List<ArmorType>();

            Equipments = new Dictionary<Slot, Item?>()
            {
                {Slot.Weapon, null },
                {Slot.Head, null },
                {Slot.Body, null },
                {Slot.Legs, null }
            };

        }

        protected void LevelUpHero(int strength, int dexterity, int intelligence)//USed in each hero child class
        {
            LevelAttribute.LevelUp(strength, dexterity, intelligence);
            level++;
        }

        public abstract void LevelUp();

        public void EquipWeapon(Weapon weapon)
        {
            if (weapon == null) throw new InvalidWeaponException();

            if (ValidWeaponTypes.Contains((WeaponType)weapon.WeaponType!) && weapon.isValidLevel(level)) //if weapon is valid add to slot
            {
                Equipments[Slot.Weapon] = weapon;
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }
        public void EquipArmor(Armor armor)
        {
            if (armor == null) throw new InvalidArmorException();

            if (ValidWeaponTypes.Contains((WeaponType)armor.armorType!) && armor.isValidLevel(level)) //if weapon is valid add to slot
            {
                Equipments[armor.ArmorSlot] = armor;
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }

        public void TotalAttributes()
        {
            int totalAttributes = LevelAttribute.GetTotalAttribute();

            foreach (KeyValuePair<Slot, Item?> armor in Equipments)
            {
                if(armor.Value != null && armor.Key != Slot.Weapon)
                {
                    totalAttributes += ((Armor)armor.Value).ArmorAttribute.GetTotalAttribute();
                }
            }

        }

        private int GetTotalStrength()
        {
            int totalStrength = LevelAttribute.Strength;

            foreach (KeyValuePair<Slot, Item?> armor in Equipments)
            {
                if (armor.Value != null && armor.Key != Slot.Weapon)
                {
                    totalStrength += ((Armor)armor.Value).ArmorAttribute.Strength;
                }
            }

            return totalStrength;
        }

        private int GetTotalDexterity()
        {
            int totalDexterity = LevelAttribute.Dexterity;

            foreach (KeyValuePair<Slot, Item?> armor in Equipments)
            {
                if (armor.Value != null && armor.Key != Slot.Weapon)
                {
                    totalDexterity += ((Armor)armor.Value).ArmorAttribute.Dexterity;
                }
            }

            return totalDexterity;
        }

        private int GetTotalIntelligence()
        {
            int totalIntelligence = LevelAttribute.Intelligence;

            foreach (KeyValuePair<Slot, Item?> armor in Equipments)
            {
                if (armor.Value != null && armor.Key != Slot.Weapon)
                {
                    totalIntelligence += ((Armor)armor.Value).ArmorAttribute.Intelligence;
                }
            }

            return totalIntelligence;
        }

        public abstract int Damage();

        public string Display()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Name: " + name);
            builder.AppendLine("Class: " + className.ToString());
            builder.AppendLine("Level: " + level);
            builder.AppendLine("Total Strength: " + GetTotalStrength());
            builder.AppendLine("Total Dexterity: " + GetTotalDexterity());
            builder.AppendLine("Total Intelligence: " + GetTotalIntelligence());
            builder.AppendLine("Damage: " + Damage());
            Console.WriteLine(builder.ToString());
            return builder.ToString();
        }
    }
}
