﻿using RPG_Assignment.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment.Classes
{
    public class Ranger : Hero
    {
        public Ranger(string name) : base(name)
        {
            LevelUpHero(1, 7, 1);
            ValidWeaponTypes.Add(WeaponType.Bow);

            ValidArmorTypes.Add(ArmorType.Leather);
            ValidArmorTypes.Add(ArmorType.Mail);
            className = ClassName.Ranger;
        }
        public override int Damage()
        {
            int damagingAttribute = LevelAttribute.Dexterity;
            if (Equipments[Slot.Weapon] != null)
            {
                return ((Weapon)Equipments[Slot.Weapon]!).WeaponDamage * (1 + damagingAttribute / 100);
            }
            else
            {
                return (1 + damagingAttribute / 100);
            }
        }
        public override void LevelUp()
        {
            LevelUpHero(1, 5, 1);
        }
    }
}
