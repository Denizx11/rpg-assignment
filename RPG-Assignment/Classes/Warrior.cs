﻿using RPG_Assignment.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment.Classes
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
            LevelUpHero(2, 6, 1);
            ValidWeaponTypes.Add(WeaponType.Axe);
            ValidWeaponTypes.Add(WeaponType.Hammer);
            ValidWeaponTypes.Add(WeaponType.Sword);

            ValidArmorTypes.Add(ArmorType.Mail);
            ValidArmorTypes.Add(ArmorType.Plate);
            className = ClassName.Warrior;
        }
        
        public override int Damage()
        {
            int damagingAttribute = LevelAttribute.Strength;
            if(Equipments[Slot.Weapon] != null)
            {
                return ((Weapon)Equipments[Slot.Weapon]!).WeaponDamage * (1 + damagingAttribute/100);
            }
            else
            {
                return (1 + damagingAttribute / 100);
            }
        }

        public override void LevelUp()
        {
            LevelUpHero(3, 2, 1);
        }
    }
}
