﻿using RPG_Assignment.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment.Classes
{
    public class Mage : Hero
    {
        
        public Mage(string name) : base(name)
        {
            LevelUpHero(1, 1, 8);
            ValidWeaponTypes.Add(WeaponType.Staff);
            ValidWeaponTypes.Add(WeaponType.Wand);
            
            ValidArmorTypes.Add(ArmorType.Cloth);
            className = ClassName.Mage;
        }
        public override int Damage()
        {
            int damagingAttribute = LevelAttribute.Intelligence;
            if (Equipments[Slot.Weapon] != null)
            {
                return ((Weapon)Equipments[Slot.Weapon]!).WeaponDamage * (1 + damagingAttribute / 100);
            }
            else
            {
                return (1 + damagingAttribute / 100);
            }
        }
        public override void LevelUp() 
        {
            LevelUpHero(1, 1, 8);

        } 
    }
}
