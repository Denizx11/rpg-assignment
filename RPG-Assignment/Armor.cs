﻿using RPG_Assignment.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assignment
{
    public class Armor : Item
    {
        public Armor(
            string name,
            int requiredLevel, 
            HeroAtrributes armorAttribute,
            ArmorType armor,
            Slot armorSlot) 
            : base(name, requiredLevel)
        {
            armorType = armor;
            ArmorAttribute = armorAttribute;
            ArmorSlot = armorSlot;
        }
       
        public ArmorType armorType { get; init; }
        public HeroAtrributes ArmorAttribute { get; set; }
        public Slot ArmorSlot { get; set; }

        
    }
}
